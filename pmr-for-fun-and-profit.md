% PMR for fun and profit
% Tobias Langner
% 2018-04-09

# PMR

## PMR
- pmr = polymorphic memory resource
- introduced by [p0339](http://open-std.org/JTC1/SC22/WG21/docs/papers/2017/p0339r3.pdf)

## What is it?
> - control allocation without affecting the type
> - `<memory_resource>` header
> - `std::pmr::polymorphic_allocator` as an allocator
> - `std::pmr::memory_resource` to provide the actual implementation

# Memory - why is new/delete not enough?

## new/delete make some implicit choices for you:

> - thread-safe
> - on the heap

. . .

## new delete doesn't know the context:

> - sizes to be allocated
> - allocation / deallocation order

## new delete does not implement all use-cases:

> - diagnosis
>     - marking memory regions
>     - providing usage information
> - special use-case
>     - initialize memory before it's used
>     - overwrite memory after it's used

# Allocators

## what is the signature of `std::vector`?

. . .

~~~{.cpp}
template<
    class T,
    class Allocator = std::allocator<T>
> class vector;
~~~

## Allocators

> - originally intended as a means to make the library more flexible and independent of the underlying memory model (Wikipedia)
> - used by STL container to manage their memory
> - until C++11 very complicated - and effectively could not have state.
> - since C++17 very easy to very complicated - depending on what you need

## The easy way

we focus on the easy way:

> - std::pmr::polymorphic_allocator and
> - std::pmr::memory_resource

# DEMO-Code

# Summary

## Summary - create a memory resource

> - derive from std::pmr::memory_resource
> - implement do_allocate, do_deallocate, do_is_equal
> - you don't have to write everything yourself, you can nest multiple memory resources
> - use std::pmr::get_default_resource() instead of hardcoding new_delete_resource if you don't need to.

--------------------

~~~{.cpp}
#include <memory_resource>

class delete_content_resource : public std::pmr::memory_resource
{
public:
	delete_content_resource(std::pmr::memory_resource* parent = std::pmr::get_default_resource())
		: parent_resource_(parent) {}
private:
	virtual void* do_allocate(std::size_t bytes, std::size_t alignment) override;
	virtual void do_deallocate(void* p, std::size_t bytes, std::size_t alignment) override;
	virtual bool do_is_equal(const std::pmr::memory_resource& other) const noexcept override;

	std::pmr::memory_resource* parent_resource_;
};
~~~

## Summary - use a memory resource

> - instantiate your memory resource
> - create a std::pmr::polymorphic_allocator with your resource
> - use that allocator in container
> - advanced: create objects using allocate() & construct() in your memory

---------------------

~~~{.cpp}
auto memory_resource = std::make_unique<my_resource>();
std::pmr::polymorphic_allocator<int> my_allocator(memory_resource.get());
std::pmr::vector<int> vec(my_allocator);
~~~

## Remarks

> - the memory_resource needs to be kept alive by YOU until it's not needed anymore
> - the polymorphic_allocator properly scopes to container in container (e.g. a std::pmr::vector\<std::pmr::string\>)

# Bonus Slides

## And what about performance?

> - performance is a good reason for using custom allocators
> - if you can exploit special properties of your problem / code
> - or to achieve cache locality
> - polymorphic allocators incure the virtual call overhead

## Demo

# Questions?

## Links

- [Allocators, the good parts (CppCon 2017)](https://www.youtube.com/watch?v=v3dz-AKOVL8)
- [Heap Layers](https://github.com/emeryberger/Heap-Layers)
- [Foonathan's memory](https://foonathan.net/doc/memory/)