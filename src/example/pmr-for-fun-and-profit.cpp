#include <memory_resource>
#include <deque>
#include <cstring>
#include <memory>
#include <string>
#include <iostream>
#include <mutex>
#include <algorithm>

#include <Windows.h>

#undef max

class delete_content_resource : public std::pmr::memory_resource
{
public:
	delete_content_resource(std::pmr::memory_resource* parent = std::pmr::get_default_resource())
		: parent_resource_(parent)
	{}
private:
	virtual void* do_allocate(std::size_t bytes, std::size_t alignment) override;
	virtual void do_deallocate(void* p, std::size_t bytes, std::size_t alignment) override;
	virtual bool do_is_equal(const std::pmr::memory_resource& other) const noexcept override;

	std::pmr::memory_resource* parent_resource_;
};

void* delete_content_resource::do_allocate(std::size_t bytes, std::size_t alignment)
{
	//get memory from parent resource
	return parent_resource_->allocate(bytes, alignment);
}

void delete_content_resource::do_deallocate(void* p, std::size_t bytes, std::size_t alignment)
{
	SecureZeroMemory(p, bytes);
	parent_resource_->deallocate(p, bytes, alignment);
}

bool delete_content_resource::do_is_equal(const std::pmr::memory_resource& other) const noexcept
{
	return &other == this;
}

class tracking_resource : public std::pmr::memory_resource
{
public:
	tracking_resource(std::pmr::memory_resource* parent = std::pmr::get_default_resource())
		: parent_resource_(parent)
		, current_(0)
		, max_(0)
		, num_allocations_(0)
		, num_open_allocations_(0)
		, average_allocation_size_(0)
	{}

	int current_memory_usage() const;
	int max_memory_usage() const;
	int number_of_allocations() const;
	bool has_open_allocations() const;
	double average_allocation_size() const;

private:
	virtual void* do_allocate(std::size_t bytes, std::size_t alignment) override;
	virtual void do_deallocate(void* p, std::size_t bytes, std::size_t alignment) override;
	virtual bool do_is_equal(const std::pmr::memory_resource& other) const noexcept override;

	std::pmr::memory_resource* parent_resource_;
	mutable std::mutex mutex_;

	int current_;
	int max_;
	int num_allocations_;
	int num_open_allocations_;
	double average_allocation_size_;
};

void* tracking_resource::do_allocate(std::size_t bytes, std::size_t alignment)
{
	//get memory from parent resource
	auto result = parent_resource_->allocate(bytes, alignment);
	{
		std::scoped_lock<std::mutex> lock(mutex_);
		num_allocations_ += 1;
		num_open_allocations_ += 1;
		average_allocation_size_ = average_allocation_size_ + (bytes - average_allocation_size_) / num_allocations_;
		current_ = current_ + static_cast<int>(bytes);
		max_ = std::max(max_, current_);
	}

	return result;
}

void tracking_resource::do_deallocate(void* p, std::size_t bytes, std::size_t alignment)
{
	parent_resource_->deallocate(p, bytes, alignment);
	{
		std::scoped_lock<std::mutex> lock(mutex_);
		current_ = current_ - static_cast<int>(bytes);
		num_open_allocations_ -= 1;
	}
}

bool tracking_resource::do_is_equal(const std::pmr::memory_resource& other) const noexcept
{
	return &other == this;
}

int tracking_resource::current_memory_usage() const
{
	std::scoped_lock<std::mutex> lock(mutex_);
	return current_;
}

int tracking_resource::max_memory_usage() const
{
	std::scoped_lock<std::mutex> lock(mutex_);
	return max_;
}

int tracking_resource::number_of_allocations() const
{
	std::scoped_lock<std::mutex> lock(mutex_);
	return num_allocations_;
}

bool tracking_resource::has_open_allocations() const
{
	std::scoped_lock<std::mutex> lock(mutex_);
	return num_open_allocations_ != 0;
}

double tracking_resource::average_allocation_size() const
{
	std::scoped_lock<std::mutex> lock(mutex_);
	return average_allocation_size_;
}

void construct()
{
	auto memory_resource = std::make_unique<tracking_resource>();
	std::pmr::polymorphic_allocator<int> tracking_allocator(memory_resource.get());
	std::pmr::vector<int>* memory = reinterpret_cast<std::pmr::vector<int>*>(tracking_allocator.allocate(sizeof(std::pmr::vector<int>)));
	tracking_allocator.construct(memory);
	std::cout << "Memory: " << memory_resource->current_memory_usage() << "\n";
	memory->push_back(5);
	std::cout << "Memory: " << memory_resource->current_memory_usage() << "\n";
}

//please note that this it not actually secure but it demonstrates the allocator
//to make that actually secure, more measures have to be taken.
void secure_allocator()
{
	auto memory_resource = std::make_unique<delete_content_resource>();
	std::pmr::polymorphic_allocator<char> secure_allocator(memory_resource.get());
	{
		std::pmr::string secure("secure data - but this string has to be longer for the small string optimization to not take effect", secure_allocator);
		std::cout << secure << "\n";
		std::cout << "Location: 0x" << std::hex << reinterpret_cast<intptr_t>(secure.data()) << std::dec << "\n";
	}
}

template <typename T>
void tracking_allocator()
{
	auto memory_resource = std::make_unique<tracking_resource>();
	std::pmr::polymorphic_allocator<int> tracking_allocator(memory_resource.get());
	{
		T container(tracking_allocator);
		for (int i = 0; i < 10'000; ++i)
		{
			container.push_back(i);
		}
		std::cout << "Number of allocations: " << memory_resource->number_of_allocations() << "\n";
		std::cout << "Current memory usage: " << memory_resource->current_memory_usage() << "\n";
		std::cout << "Max memory usage: " << memory_resource->max_memory_usage() << "\n";
		container.clear();
		std::cout << "\nContainer cleared\n";
		std::cout << "Current memory usage: "<< memory_resource->current_memory_usage() <<"\n";
	}
	std::cout << "\nContainer destroyed\n";
	std::cout << "Current memory usage: " << memory_resource->current_memory_usage() << "\n";
	std::cout << "Avg allocation size:  " << memory_resource->average_allocation_size() << "\n";
}

int main()
{
	construct();
	std::cout << "\n-----------------------------------------------------\n";
	secure_allocator();
	std::cout << "\n-----------------------------------------------------\n";
	std::cout << "Vector:\n";
	tracking_allocator<std::pmr::vector<int>>();

	std::cout << "\n-----------------------------------------------------\n";
	std::cout << "Deque:\n";
	tracking_allocator<std::pmr::deque<int>>();

	std::cout << "\n-----------------------------------------------------\n";
	std::cout << "List:\n";
	tracking_allocator<std::pmr::list<int>>();

	return 0;
}
