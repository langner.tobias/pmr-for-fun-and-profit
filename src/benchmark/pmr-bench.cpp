#include <memory_resource>
#include <memory>
#include <list>
#include "benchmark/benchmark.h"

class stack_resource : public std::pmr::memory_resource
{
public:
  stack_resource(size_t bytes);

  virtual void* do_allocate(std::size_t bytes, std::size_t alignment) override;
  virtual void do_deallocate(void* p, std::size_t bytes, std::size_t alignment) override;
  virtual bool do_is_equal(const std::pmr::memory_resource& other) const noexcept override;

private:
  std::unique_ptr<char[]> heap_;
  char* position_;
  char* end_;
};

stack_resource::stack_resource(size_t bytes)
	: heap_(new char[bytes])
	, position_(heap_.get())
	, end_(position_ + bytes)
{}

void* stack_resource::do_allocate(std::size_t bytes, std::size_t alignment)
{
	char* new_pos = position_ + bytes + bytes % alignment;

	if (new_pos > end_) throw std::bad_alloc();

	void* result = position_;
	position_ = new_pos;
	return result;
}

void stack_resource::do_deallocate(void* , std::size_t , std::size_t )
{
	//do nothing
}

bool stack_resource::do_is_equal(const std::pmr::memory_resource& other) const noexcept
{
	return &other == this;
}

void ListFill_std_allocator(benchmark::State& state) {
  std::list<int> list;
  int count = 0;
  // Code inside this loop is measured repeatedly
  for (auto _ : state) {
    list.push_back(count);
    ++count;
  }
}
// Register the function as a benchmark
BENCHMARK(ListFill_std_allocator);

void ListFill_poly_allocator(benchmark::State& state) {
	stack_resource resource(1024*1024*1024);
	std::pmr::polymorphic_allocator<int> poly(&resource);
	std::pmr::list<int> list(poly);
	int count = 0;
	// Code inside this loop is measured repeatedly
	for (auto _ : state) {
		list.push_back(count);
		++count;
	}
}
// Register the function as a benchmark
BENCHMARK(ListFill_poly_allocator);

BENCHMARK_MAIN();


