add_executable(pmr-bench "")
target_sources(pmr-bench
	PRIVATE
		pmr-bench.cpp)

target_link_libraries(pmr-bench benchmark)